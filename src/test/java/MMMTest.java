import com.binar.ModelImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MMMTest {
    ModelImpl model;
    ArrayList listArr1, listArr2, listNullArr = null;
    Map<Integer, Integer> mMap1, mMap2;
    //Unit Test for Mean
    @Test
    @DisplayName("Positive Test - mean")
    void MeanPositive() {
        var expectedMean1 = 16.75d;
        var expectedMean2 = 8.32d;
        Assertions.assertEquals(expectedMean1, model.mean(listArr1));
        Assertions.assertEquals(expectedMean2, model.mean(listArr2));
    }

    @Test
    @DisplayName("Negative Test - mean")
    void MeanNegative() {
        Assertions.assertThrows(NullPointerException.class, () -> model.mean(listNullArr));
    }

    //Unit Test for Median
    @Test
    @DisplayName("Positive Test - median")
    void MedianPositive() {
        var expectedMedian1 = 15.5d;
        var expectedMedian2 = 8.0d;
        Assertions.assertEquals(expectedMedian1, model.median(listArr1));
        Assertions.assertEquals(expectedMedian2, model.median(listArr2));
    }

    @Test
    @DisplayName("Negative Test - median")
    void MedianNegative() {
        Assertions.assertThrows(NullPointerException.class, () -> model.median(listNullArr));
    }

    //Unit Test for Modus
    @Test
    @DisplayName("Positive Test - modus")
    void ModusPositive() {
        var expectedModus1 = 13;
        var expectedModus2 = 7;
        Assertions.assertEquals(expectedModus1, model.modus(listArr1));
        Assertions.assertEquals(expectedModus2, model.modus(listArr2));
    }

    @Test
    @DisplayName("Negative Test - modus")
    void ModusNegative() {
        Assertions.assertThrows(NullPointerException.class, () -> model.modus(listNullArr));
    }

    //Unit Test for Frekuensi
    @Test
    @DisplayName("Positive Test - frekuensi")
    void FrekuensiPositive() {
        declareMap();
        Assertions.assertEquals(mMap1, model.frekuensi(listArr1));
        Assertions.assertEquals(mMap2, model.frekuensi(listArr2));
    }

    @Test
    @DisplayName("Negative Test - frekuensi")
    void FrekuensiNegative() {
        Assertions.assertThrows(NullPointerException.class, () -> model.frekuensi(listNullArr));
    }

    void declareMap() {
        Integer[] keys1 = {9, 10, 12, 13, 15, 16, 18, 22, 23, 24, 25};
        Integer[] values1 = {1, 1, 1, 3, 2, 2, 1, 1, 1, 2, 1};
        mMap1 = IntStream.range(0, keys1.length).boxed()
                .collect(Collectors.toMap(i -> keys1[i], i -> values1[i]));

        Integer[] keys2 = {5, 6, 7, 8, 9, 10};
        Integer[] values2 = {1, 2, 62, 42, 50, 41};
        mMap2 = IntStream.range(0, keys2.length).boxed()
                .collect(Collectors.toMap(i -> keys2[i], i -> values2[i]));
    }
}
