import com.binar.ModelImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

public class ReadWriteTest {
    ModelImpl model;
    ArrayList listArr1, listArr2, listNullArr = null;
    Map<Integer, Integer> mMap1, mMap2;

    String trueReadPath = "src/main/resources/data_sekolah.csv";
    String wrongReadPath = "src/main/resources/data_not_found.csv";
    String saveMeanMedianModus = "src/main/resources/MeanMedianModus.txt";
    String saveFrekuensi = "src/main/resources/Frekuensi.txt";

    @BeforeEach
    void setup() {
        model = new ModelImpl();
        declareListAngka();
    }

    void declareListAngka() {
        Integer[] arrList1 = {9, 10, 12, 13, 13, 13, 15, 15, 16, 16, 18, 22, 23, 24, 24, 25};
        listArr1 = new ArrayList<>(Arrays.asList(arrList1));

        Integer[] arrList2 = {5,
                6, 6,
                7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
                7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
                7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
                7, 7, 7, 7, 7, 7, 7, 7,
                8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
                8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
                8, 8, 8, 8, 8, 8,
                9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
                9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
                9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
                10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
                10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
                10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10
        };
        listArr2 = new ArrayList<>(Arrays.asList(arrList2));
    }

    //Unit Test for Read File
    @Test
    @DisplayName("Positive Test - readFile")
    void readFilePositive() {
        checkIfReadFileNotThrow();
        checkIfReadFileWorks_ReturnArrayList();
    }

    void checkIfReadFileNotThrow() {
        Assertions.assertDoesNotThrow(() -> model.readFile(trueReadPath));
    }

    void checkIfReadFileWorks_ReturnArrayList() {
        try {
            Assertions.assertEquals(listArr2, model.readFile(trueReadPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Negative Test - readFile")
    void readFileNegative() {
        Assertions.assertThrows(IOException.class, () -> model.readFile(wrongReadPath));
    }

    //Unit Test for write Mean Median Modus File
    @Test
    @DisplayName("Positive Test - writeMeanMedianModusFile")
    void writeMeanMedianModusFilePositive() {
        checkIfWriteMMMFileNotThrow();
        runWriteMMMFile();
        checkIfMMMFileIsCreated();
    }

    void checkIfMMMFileIsCreated() {
        File file = new File(saveMeanMedianModus);
        Assertions.assertTrue(file.exists());
    }

    void runWriteMMMFile() {
        try {
            model.writeMeanMedianModus(saveMeanMedianModus, model.readFile(trueReadPath));
        } catch (IOException e) {

        }
    }

    void checkIfWriteMMMFileNotThrow() {
        Assertions.assertDoesNotThrow(() -> model.writeMeanMedianModus(saveMeanMedianModus, model.readFile(trueReadPath)));
    }

    @Test
    @DisplayName("Negative Test - writeMeanMedianModus")
    void writeMeanMedianModusNegative() {
        Assertions.assertThrows(Exception.class, () -> model.writeMeanMedianModus(saveMeanMedianModus, listNullArr));
    }

    //Unit Test for write Frekuensi File
    @Test
    @DisplayName("Positive Test - writeFrekuensi")
    void writeFrekuensiPositive() {
        checkIfWriteFrekuensiFileNotThrow();
        runWriteFrekuensiFile();
        checkIfWriteFrekuensiFileIsCreated();
    }

    void checkIfWriteFrekuensiFileIsCreated() {
        File file = new File(saveFrekuensi);
        Assertions.assertTrue(file.exists());
    }

    void runWriteFrekuensiFile() {
        try {
            model.writeFrekuensi(saveFrekuensi, model.readFile(trueReadPath));
        } catch (IOException e) {

        }
    }

    void checkIfWriteFrekuensiFileNotThrow() {
        Assertions.assertDoesNotThrow(() -> model.writeFrekuensi(saveMeanMedianModus, model.readFile(trueReadPath)));
    }

    @Test
    @DisplayName("Negative Test - writeFrekuensi")
    void writeFrekuensiNegative(){
        Assertions.assertThrows(Exception.class, () -> model.writeFrekuensi(saveMeanMedianModus, listNullArr));
    }
}
