package com.binar;

import java.util.List;
import java.util.Map;

public interface Model {
    double mean(List<Integer> list);

    double median(List<Integer> list);

    int modus(List<Integer> list);

    Map<Integer, Integer> frekuensi(List<Integer> list);
}
