package com.binar;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class MenuImpl{
    ModelImpl model = new ModelImpl();
    Scanner in = new Scanner(System.in);

    public static String CSV_Path = "src/main/resources/data_sekolah.csv"; //read path csv
    public static String MeanMedianModus_Path = "src/main/resources/MeanMedianModus.txt"; //read path mean median modus
    public static String Frekuensi_Path = "src/main/resources/Frekuensi.txt"; //read path frekuensi

    public void pilihMenu(int pilih){
        switch (pilih) {
            case 0:
                System.out.println("Aplikasi ditutup, terima kasih!");
                System.exit(0);
                break;
            case 1:
                MeanMedianModus();
                pilihMenu(menu2.tampilanMenu());
                break;
            case 2:
                Frekuensi();
                pilihMenu(menu2.tampilanMenu());
                break;
            case 3:
                MeanMedianModus();
                Frekuensi();
                pilihMenu(menu2.tampilanMenu());
                break;
            default:
                System.out.println("Mohon maaf, pilih angka kembali");
                pilihMenu(menu.tampilanMenu());
                break;
        }
    }

    Menu menu = () -> {
        menuUtama();
        return input();
    };

    Menu menu2 = () -> {
        menuKedua();
        return input2();
    };

    public void menuUtama(){
        System.out.println("--------------------------------");
        System.out.println("Aplikasi Pengolah Nilai Siswa");
        System.out.println("--------------------------------");
        System.out.println("" +
                "1. Generate txt Mean, Median, Modus " +
                "\n2. Generate txt Modus Sekolah " +
                "\n3. Generate kedua file " +
                "\n0. Keluar");
        System.out.print("---->");
    }

    public void menuKedua(){
        System.out.println("1. Kembali" +
                "\n0. Keluar");
        System.out.print("---->");
    }

    public void MeanMedianModus(){
        try {
            model.writeMeanMedianModus(MeanMedianModus_Path, model.readFile(CSV_Path));
        } catch (IOException e) {
            System.err.println("File direktori tidak dapat ditemukan");
        }
    }

    public void Frekuensi(){
        try {
            model.writeFrekuensi(Frekuensi_Path, model.readFile(CSV_Path));
        } catch (IOException e) {
            System.err.println("File direktori tidak dapat ditemukan");
        }
    }
    public int input(){
        int pilih = 0;
        try {
            pilih = in.nextInt();
        } catch (InputMismatchException e) {
            System.err.println("Silahkan pilih kembali");
        }
        return pilih;
    }

    public int input2(){
        int pilih = 0;
        try {
            pilih = in.nextInt();
            if(pilih == 0){
                System.out.println("Aplikasi ditutup, terima kasih!");
                System.exit(0);
            } else if (pilih == 1){
                pilihMenu(menu.tampilanMenu());
            }
        } catch (InputMismatchException e) {
            System.err.println("Silahkan pilih kembali");
        }
        return pilih;
    }
}
