package com.binar;

import java.io.*;
import java.util.*;

public class ModelImpl implements Model {
    public void writeMeanMedianModus(String saveFile, List<Integer> read) throws IOException {
        File file = new File(saveFile);
        if (file.createNewFile()){
            System.out.println("File Mean Median Modus tersimpan di direktori " + saveFile);
        } else if (file.exists()){
            System.out.println("File Mean Median Modus telah diupdate dan tersimpan kembali di" + saveFile);
        }
        FileWriter fw = new FileWriter(file);
        BufferedWriter writer = new BufferedWriter(fw);

        writer.write("Hasil Pengolahan Nilai\n");
        writer.newLine();
        writer.write("Data Nilai:");
        writer.newLine();

        //Mean
        writer.write("Mean   : " + mean(read));
        writer.newLine();

        //Median
        if (median(read) % 1 == 0) {
            var med = (int) median(read);
            writer.write("Median : " + med);
            writer.newLine();

        } else {
            writer.write("Median : " + median(read));
            writer.newLine();
        }
        //Modus
        writer.write("Modus  : " + modus(read));
        writer.newLine();
        writer.flush();
        writer.close();
    }

    public void writeFrekuensi(String saveFile, List<Integer> read) throws IOException {
        File file = new File(saveFile);
        if (file.createNewFile()) {
            System.out.println("File Frekuensi  tersimpan di " + saveFile);
        } else if (file.exists()) {
            System.out.println("File Frekuensi telah diupdate dan tersimpan kembali di " + saveFile);
        }
        FileWriter fw = new FileWriter(file);
        BufferedWriter writer = new BufferedWriter(fw);

        var hMap = frekuensi(read);
        Set<Integer> key = hMap.keySet();

        writer.write("Hasil Pengolahan Nilai\n");
        writer.newLine();
        writer.write("Data Nilai:");
        writer.newLine();
        writer.write("Nilai\t\t\t" + "|\t\t\t" + "Frekuensi" + "\n");

        for (Integer nilai : key) {
            writer.write(nilai + "\t\t\t\t" + "|\t\t\t" + hMap.get(nilai) + "\n");
        }
        writer.flush();
        writer.close();
    }
        public List<Integer> readFile(String path) throws IOException {

            File file = new File(path);
            FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr);

            var line = "";
            String[] tempArr;

            List<Integer> listInt = new ArrayList<>();

            while ((line = reader.readLine()) != null) {
                tempArr = line.split(";");

                for (int i = 0; i < tempArr.length; i++) {
                    if (i == 0) {

                    } else {
                        var temp = tempArr[i];
                        var intTemp = Integer.parseInt(temp);
                        listInt.add(intTemp);
                    }
                }
            }
            Collections.sort(listInt);
            reader.close();
            return listInt;
        }

    @Override
    public double mean(List<Integer> list) {
        var hasil = list.stream()
                .mapToDouble(d -> d)
                .average()
                .orElse(0.0);
        return Double.parseDouble(String.format("%.2f", hasil));
    }

    @Override
    public double median(List<Integer> list) {
        double median;
        if (list.size() % 2 == 0)
            median = ((double) list.get(list.size() / 2) + (double) list.get(list.size() / 2 - 1)) / 2;
        else
            median = (double) list.get(list.size() / 2);
        return median;
    }

    @Override
    public int modus(List<Integer> list) {
        HashMap<Integer, Integer> hm = new HashMap<>();
        int max = 1;
        int temp = 0;

        for (Integer integer : list) {

            if (hm.get(integer) != null) {

                int count = hm.get(integer);
                count++;
                hm.put(integer, count);

                if (count > max) {
                    max = count;
                    temp = integer;
                }
            } else
                hm.put(integer, 1);
        }
        return temp;
    }

    @Override
    public Map<Integer, Integer> frekuensi(List<Integer> list) {
        Set<Integer> distinct = new HashSet<>(list);
        Map<Integer, Integer> mMap = new HashMap<>();

        for (Integer s : distinct) {
            mMap.put(s, Collections.frequency(list, s));
        }
        return mMap;
    }
}
